/// Copyright (c) 2018 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  
  static let geoCoder = CLGeocoder()
  let center = UNUserNotificationCenter.current()
  var locationManager:CLLocationManager? = CLLocationManager()
  var lastLocation:CLLocation?
  var myLocation:CLLocation?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    let rayWenderlichColor = UIColor(red: 0/255, green: 104/255, blue: 55/255, alpha: 1)
    UITabBar.appearance().tintColor = rayWenderlichColor
    
    center.requestAuthorization(options: [.alert, .sound]) { granted, error in
    }
    locationManager!.requestAlwaysAuthorization()
    
    /*
    locationManager.startMonitoringVisits()
    locationManager.delegate = self
    
    // Uncomment following code to enable fake visits
    locationManager.distanceFilter = 10 // 0
    locationManager.allowsBackgroundLocationUpdates = true // 1
    locationManager.startUpdatingLocation()  // 2
    */
    
    if launchOptions?[UIApplication.LaunchOptionsKey.location] != nil {
               print("Terminated location found")
               if locationManager == nil {
                   print("Terminated location manager nil found")
                   locationManager?.startMonitoringVisits()
                   locationManager = CLLocationManager()
                   locationManager?.delegate = self
                   locationManager?.distanceFilter = 500
                   locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
                   locationManager?.allowsBackgroundLocationUpdates = true
                   locationManager?.startUpdatingLocation()
               } else {
                   print("Terminated location manager found")
                   locationManager = nil
                   locationManager = CLLocationManager()
                   locationManager?.delegate = self
                   locationManager?.startMonitoringVisits()
                   locationManager?.distanceFilter = 500
                   locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
                   locationManager?.allowsBackgroundLocationUpdates = true
                   locationManager?.startUpdatingLocation()
               }
           } else {
               locationManager?.delegate = self
               locationManager?.distanceFilter = 500
               locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
               locationManager?.allowsBackgroundLocationUpdates = true
               locationManager?.startUpdatingLocation()  // 2
        
               if CLLocationManager.authorizationStatus() == .notDetermined {
                   locationManager?.requestAlwaysAuthorization()
               }
               else if CLLocationManager.authorizationStatus() == .denied {
                   print("User denied location services")
               }
               else if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                   locationManager?.requestAlwaysAuthorization()
               }
               else if CLLocationManager.authorizationStatus() == .authorizedAlways {
                   locationManager?.startUpdatingLocation()
               }
           }
    
    return true
  }
}

extension AppDelegate: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
    // create CLLocation from the coordinates of CLVisit
    let clLocation = CLLocation(latitude: visit.coordinate.latitude, longitude: visit.coordinate.longitude)
    
    // Get location description
    AppDelegate.geoCoder.reverseGeocodeLocation(clLocation) { placemarks, _ in
      if let place = placemarks?.first {
        let description = "\(place)"
        self.newVisitReceived(visit, description: description)
      }
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
     print("Exited Region")

    locationManager?.stopMonitoring(for: region)
       //Start location manager and fetch current location
    locationManager?.startUpdatingLocation()
  }
  
  func newVisitReceived(_ visit: CLVisit, description: String) {
    let location = Location(visit: visit, descriptionString: description)
    LocationsStorage.shared.saveLocationOnDisk(location)
    
    let content = UNMutableNotificationContent()
    content.title = "New Journal entry 📌"
    content.body = location.description
    content.sound = UNNotificationSound.default
    
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    let request = UNNotificationRequest(identifier: location.dateString, content: content, trigger: trigger)
    
    center.add(request, withCompletionHandler: nil)
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
    print("location.timestamp :",location.timestamp)
    AppDelegate.geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
      if let place = placemarks?.first {
        let description = "Fake visit: \(place)"
        var distanceInMeteres: CLLocationDistance = CLLocationDistance()
        print("didUpdateLocations \(location.coordinate) and \(location.horizontalAccuracy)")
                  
                  if (location.horizontalAccuracy) <= Double(65.0) {

                    if let coordiate0 = self.myLocation {
                        let coordinate₁ = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                        distanceInMeteres = coordiate0.distance(from: coordinate₁) as! CLLocationDistance
                      print("distanceInMeteres :",distanceInMeteres)

                    }
                    self.myLocation = location
                      if !(UIApplication.shared.applicationState == .active) {
                          self.createRegion(location: location)
                      }
                    // call api to send location
                    self.sendCoordiates(location: location)
                  } else {
                      manager.stopUpdatingLocation()
                      manager.startUpdatingLocation()
                  }
        let fakeVisit = FakeVisit(coordinates: location.coordinate, arrivalDate: Date(), departureDate: Date())
        self.newVisitReceived(fakeVisit, description: "distance between 2 points: \(distanceInMeteres.description)")
      }
    }
  }
  
  
  
  func applicationDidEnterBackground(_ application: UIApplication) {
      //Create a region
    self.createRegion(location: myLocation)
  }
  
  func createRegion(location:CLLocation?) {
      guard let location = location else {
          print("Problem with location in creating region")
          return
      }
      if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
          
          let coordinate = CLLocationCoordinate2DMake((location.coordinate.latitude), (location.coordinate.longitude))
          let regionRadius = 50.0
          
          let region = CLCircularRegion(center: CLLocationCoordinate2D(
              latitude: coordinate.latitude,
              longitude: coordinate.longitude),
                                        radius: regionRadius,
                                        identifier: "aabb")
          
          region.notifyOnEntry = true
          region.notifyOnExit = true
          
          print("Region Created \(location.coordinate) with \(location.horizontalAccuracy)")
          self.locationManager?.stopUpdatingLocation()
          print("stopUpdatingLocation")
          self.locationManager?.startMonitoring(for: region)
          print("startMonitoring")
      }
      else {
          print("System can't track regions")
      }
  }

  func applicationWillTerminate(_ application: UIApplication) {
     //Create a region
    print("applicationWillTerminate")
    self.createRegion(location: myLocation)
    
  }
    
  func sendCoordiates(location: CLLocation)  {
        var body = Dictionary<String, Any>()
      
      body["messageType"] = "messageType";
      body["fourKitesLoadId"] = "fourKitesLoadId"
      body["loadNumber"] = "loadNumber"
      body["proNumber"] = "proNumber"
      body["shipper"] = "shipper"
      body["location"] = "location"
      body["city"] = "city"
      body["state"] = "state"
      body["country"] = "country"
      body["latitude"] =  location.coordinate.latitude
      body["longitude"] = location.coordinate.longitude
      body["temperatureReading"] = "temperatureReading"
      body["temperatureUnit"] = "temperatureUnit"
      body["sensor"] = "sensor"
      body["odometerReading"] = "odometerReading"
      body["timestamp"] = "timestamp"
      body["timezone"] = "timezone"
      body["timezoneShortName"] = "timezoneShortName"
      body["timezoneOffset"] = "timezoneOffset"


        let userCredentials = "DL_IOT:Dltlabs@123"
        let loginData = userCredentials.data(using: String.Encoding.utf8)!
        let basicAuth = loginData.base64EncodedString()

        let url = URL(string : "https://qa-iot.dltlabs.com/api/1.0/message")
        var request = URLRequest(url: url!)
        request.httpMethod =     "POST"
        request.addValue("Basic \(basicAuth)", forHTTPHeaderField: "authorization")
        
    do {
       request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
    } catch let error {
        print(error.localizedDescription)
    }


        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request)
        { (Data,URLResponse,Error) in

            if(Error != nil)
            {
                print("Error: \(Error)")
            }
            else{
                do{
                   print("data: \(Data)")
                   if let responseData = try JSONSerialization.jsonObject(with: Data!, options: []) as? [String: Any] {
                       // try to read out a string array
                    print("responseDataSent: \(responseData)")
                   }

                    print("URLResponse: \(URLResponse)")
                }
                catch{
                    print("Error")
                }
            }
        }
        task.resume()
    }
}

final class FakeVisit: CLVisit {
  private let myCoordinates: CLLocationCoordinate2D
  private let myArrivalDate: Date
  private let myDepartureDate: Date

  override var coordinate: CLLocationCoordinate2D {
    return myCoordinates
  }
  
  override var arrivalDate: Date {
    return myArrivalDate
  }
  
  override var departureDate: Date {
    return myDepartureDate
  }
  
  init(coordinates: CLLocationCoordinate2D, arrivalDate: Date, departureDate: Date) {
    myCoordinates = coordinates
    myArrivalDate = arrivalDate
    myDepartureDate = departureDate
    super.init()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
